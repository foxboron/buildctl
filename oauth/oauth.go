package oauth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os/exec"
	"time"
)

var (
	keycloakUrl = "https://accounts.archlinux.org/auth/realms/archlinux/protocol/openid-connect"
	redirectUrl = "http://127.0.0.1:5000"
	clientId    = "openid_buildbot"
	tokenPath   = "token"
	oauthScope  = "openid"
	userInfo    = "userinfo"
)

type AuthResponse struct {
	AccessToken string `json:"access_token"`
}

type UserInfo struct {
	Roles             []string `json:"roles"`
	Name              string   `json:"name"`
	PreferredUsername string   `json:"preferred_username"`
	Email             string   `json:"email"`
}

func maybeXdgOpen(url string) bool {
	binary, err := exec.LookPath("xdg-open")
	if err != nil {
		return false
	}
	cmd := exec.Command(binary, url)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

func loginKeycloak() (string, error) {
	values := url.Values{}
	values.Add("client_id", clientId)
	values.Add("redirect_uri", redirectUrl)
	values.Add("response_type", "code")
	values.Add("scope", oauthScope)
	values.Add("state", fmt.Sprintf("redirect=%s", redirectUrl))
	query := values.Encode()
	browseUrl := fmt.Sprintf("%s/auth?%s", keycloakUrl, query)

	listener, err := net.Listen("tcp", "127.0.0.1:5000")
	if err != nil {
		return "", err
	}

	doneCh := make(chan string)
	errCh := make(chan error)
	m := http.NewServeMux()
	s := &http.Server{
		Addr:    "127.0.0.1:5000",
		Handler: m,
	}

	m.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "<script>window.close()</script>")
		fmt.Fprint(w, "<h1>Close this tab</h1>")
		doneCh <- r.FormValue("code")
	})
	go func() {
		if err := s.Serve(listener); err != nil && !errors.Is(err, http.ErrServerClosed) {
			errCh <- err
		}
	}()
	defer func() {
		_ = s.Shutdown(context.Background())
	}()

	if !maybeXdgOpen(browseUrl) {
		fmt.Printf("Open this url in your browser: %s\n", browseUrl)
	}

	timeoutCh := time.NewTimer(120 * time.Second)
	select {
	case code := <-doneCh:
		fmt.Println("Successfully Authenticated to Keycloak!")
		return code, nil
	case err := <-errCh:
		return "", err
	case <-timeoutCh.C:
		break
	}
	close(errCh)
	close(doneCh)
	return "", fmt.Errorf("timeout requesting secret")
}

func GetKeycloakAccessToken(code string) (string, error) {
	var accDetails AuthResponse
	browseUrl := fmt.Sprintf("%s/token", keycloakUrl)
	client := &http.Client{}
	f := url.Values{}
	f.Add("client_id", clientId)
	f.Add("code", code)
	f.Add("grant_type", "authorization_code")
	f.Add("redirect_uri", "http://127.0.0.1:5000")
	resp, err := client.PostForm(browseUrl, f)
	if err != nil {
		return "", err
	}
	d := json.NewDecoder(resp.Body)
	d.Decode(&accDetails)
	return accDetails.AccessToken, nil
}

func RunAuth() (string, error) {
	code, err := loginKeycloak()
	if err != nil {
		return "", err
	}
	accessToken, err := GetKeycloakAccessToken(code)
	if err != nil {
		return "", err
	}
	return accessToken, nil
}

func GetUserinfo(token string) (*UserInfo, error) {
	var userinfo UserInfo
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://accounts.archlinux.org/auth/realms/archlinux/protocol/openid-connect/userinfo", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	d := json.NewDecoder(resp.Body)
	d.Decode(&userinfo)
	return &userinfo, nil
}
