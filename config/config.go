package config

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"path"
)

type Build struct {
	BuildId string
}

type CacheConfig struct {
	SessionToken string `json"sessiontoken"`
	Builds       []Build
}

type Config struct {
	Email         string   `json:"email"`
	Username      string   `json:"username"`
	Architectures []string `json:"architectures"`
}

var (
	configPath = ".config/archlinux/buildctl.json"
	cachePath  = ".cache/archlinux/buildctl.json"
)

func Mkdir(file string) error {
	path := path.Dir(file)
	return os.MkdirAll(path, os.ModePerm)
}

func GetPath(file string) string {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	return path.Join(dirname, file)
}

func GetCacheConfig() (*CacheConfig, error) {
	cacheFile := GetPath(cachePath)
	if _, err := os.Stat(cacheFile); errors.Is(err, os.ErrNotExist) {
		return nil, err
	}
	var cache CacheConfig
	f, err := os.ReadFile(cacheFile)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(f, &cache)
	if err != nil {
		return nil, err
	}
	return &cache, nil
}

func GetConfig() (*Config, error) {
	confFile := GetPath(configPath)
	if _, err := os.Stat(confFile); errors.Is(err, os.ErrNotExist) {
		return nil, err
	}
	var config Config
	f, err := os.ReadFile(confFile)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(f, &confFile)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func SaveConfig(conf *Config) error {
	confFile := GetPath(configPath)
	err := Mkdir(confFile)
	if err != nil {
		return err
	}
	b, err := json.Marshal(&conf)
	if err != nil {
		return err
	}
	return os.WriteFile(confFile, b, os.ModePerm)
}

func SaveCache(cache *CacheConfig) error {
	cacheFile := GetPath(cachePath)
	err := Mkdir(cacheFile)
	if err != nil {
		return err
	}
	b, err := json.Marshal(&cache)
	if err != nil {
		return err
	}
	return os.WriteFile(cacheFile, b, os.ModePerm)
}
