package main

import (
	"github.com/spf13/cobra"
	"gitlab.archlinux.org/foxboron/buildctl/buildbot"
	"gitlab.archlinux.org/foxboron/buildctl/config"
)

var build = &cobra.Command{
	Use:   "build",
	Short: "build package",
	RunE:  buildCmd,
}

func buildCmd(cmd *cobra.Command, args []string) error {
	conf, err := config.GetCacheConfig()
	if err != nil {
		return err
	}

	buildbot.ScheduleBuild(conf.SessionToken)
	return nil
}

func init() {
	CliCommands = append(CliCommands, cliCommand{
		Cmd: build,
	})
}
