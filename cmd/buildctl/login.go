package main

import (
	"github.com/spf13/cobra"
	"gitlab.archlinux.org/foxboron/buildctl/buildbot"
	"gitlab.archlinux.org/foxboron/buildctl/config"
	"gitlab.archlinux.org/foxboron/buildctl/oauth"
)

var login = &cobra.Command{
	Use:   "login",
	Short: "Login to buildbot",
	RunE:  loginCmd,
}

func loginCmd(cmd *cobra.Command, args []string) error {
	token, err := oauth.RunAuth()
	if err != nil {
		return err
	}
	user, err := oauth.GetUserinfo(token)
	if err != nil {
		return err
	}
	u := &config.Config{
		Username: user.PreferredUsername,
		Email:    user.Email,
	}
	err = config.SaveConfig(u)
	if err != nil {
		return err
	}
	cookie, err := buildbot.GetSessionToken(token)
	if err != nil {
		return err
	}
	c := &config.CacheConfig{
		SessionToken: cookie,
	}
	err = config.SaveCache(c)
	if err != nil {
		return err
	}
	return nil
}

func init() {
	CliCommands = append(CliCommands, cliCommand{
		Cmd: login,
	})
}
